Studentnummer:331375
Naam:Priscilla Ache

Hoe open je de Terminal in Visual Studio Code?
	# Optie balk > Terminal > Open Terminal of Ctrl + Shift + '

Met welk commando kan je checken of welke bestanden zijn toegevoegd aan de commit en welke niet?
	# Git status

Wat is het commando van een multiline commit message?
	#Type: Addition, Change:  .....................  , Why: No base styling was present. 


Hoeveel commando's heb je in opdracht 4a uitgevoerd?
	#8

Zoek het volgende commando op:
 - 1 commit teruggaan in de commit history. (reset)
	#eerst git log (daarbij krijg je de commit code)
	dan git reset --hard (en dan die code)

